﻿package sample;

import static java.lang.System.*;

public class Convert {
    public static void main(String[] args) {

        int a = 1;

        String b = Integer.toString(a);
        System.out.println("int to string: " + b);

        int c = Integer.parseInt(b);
        System.out.println("vise versa: " + c);

        double d = 2.1;

        String e = Double.toString(d);
        System.out.println("double to string: " + e);

        double f = Double.parseDouble(e);
        System.out.println("string to double: " + f);

        float g = (float) 4.5;

        String i = Float.toString(g);
        System.out.println("float to string: " + i);

        float j = Float.parseFloat(i);
        System.out.println("string to float: " + j);

        long k = 1234567890L;

        String l = Long.toString(k);
        System.out.println("long to string: " + l);

        long m = Long.parseLong(l);
        System.out.println("string to long: " + m);

        boolean n = true;

        String o = Boolean.toString(n);
        System.out.println("boolean to string: " + o);

        boolean p = Boolean.parseBoolean(o);
        System.out.println("string to boolean: " + o);

        char q = 's';

        String r = Character.toString(q);
        System.out.println("char to string: " + r);

        char s = r.charAt(0);
        System.out.println(" string to char: " + s);
    }

    public static class DoubleToInt {
        public static void main(String args[]) {
            double t = 111.245;
            long u = (long) t;
            int v = (int) t;
            System.out.println("\n double: " + t);
            System.out.println(" double to int: " + u);
            System.out.println(" double to long: " + v);
        }

        public static class Algebra {
            public static void main(String args[]) {
                int a = 1 + 1;
                int b = a * 3;
                int c = b / 4;
                int d = b - a;
                int e = -d;
                System.out.println("a =  " + a++);
                System.out.println("b =  " + ++b);
                System.out.println("c =  " + --c);
                System.out.println("d =  " + d--);
                System.out.println("e =  " + e);

            }

            public static class Boolean {
                public static void main(String args[]) {
                    boolean a = true;
                    boolean b;
                    b = a || true; 
                    b = !b; 
                    System.out.println(b); 
                    a = a || b;  
                    boolean c;
                    c = a && (a||b); 
                    System.out.println(c);

                }
            }
        }

    }
}




